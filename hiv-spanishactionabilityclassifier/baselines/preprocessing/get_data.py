from utils import gen_tr_dev_splits
import pandas as pd

def get_train_test_split():
    ### Read the data file in
    data = pd.read_csv("../../data/deepl/translated_tweets.csv")
    ### label-data: b_actionable
    ## move tweet_text_x -> text
    data = data.rename(columns={'tweet_text_x': 'text', 'tweet_id': 'id'})
    ## we need to drop columns with nan in text column
    data = data[data['text'].notna()]
    ## how many messages in total are actionable?
    actionable = data[data['b_actionable'] == 1]
    print(len(actionable))
    print("that was before any splitting")
    df_train, df_test = gen_tr_dev_splits(data, label_data='b_actionable', drop_na=True)
    ## how many are actionable though???
    actionable = df_train[df_train['b_actionable'] == 1]
    not_actionable = df_train[df_train['b_actionable'] == 0]
    print(len(not_actionable))
    print(len(actionable))
    df_train.to_csv("../../data/train_data.csv")
    df_test.to_csv("../../data/test_data.csv")

if __name__ == "__main__":
    get_train_test_split()
