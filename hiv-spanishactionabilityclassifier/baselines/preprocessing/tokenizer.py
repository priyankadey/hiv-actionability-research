import csv, os
import pandas as pd
import numpy as np
import sys
from utils import print_elapsed_time, clean_tweets, tokenize_tweets
import time


csv.field_size_limit(sys.maxsize)
class DataReader:
    def __init__(self, path2data, label_column='content_behavior', tweet_content = 'text', tweet_id = 'id', override_lab_col = False):
        self.path2data=path2data
        self.labcol=label_column
        self.tweet_content = tweet_content
        self.tweet_id = tweet_id
        self.tknzed_tweets = 'preproc_tweets'
        self.override_label_col = override_lab_col
        self.data = self.read()



    # assuming one type of datafile?
    # create training/dev splits?
    def read(self):
        data = pd.read_csv(self.path2data, engine='python', dtype={self.tweet_id:str})
        print(data.head(10))
        print(data.dtypes)
        # if it doesn't have a label column... just invent one
        if self.override_label_col or self.labcol not in data.columns:
            data[self.labcol] = 0


        data[self.tweet_content] = data[self.tweet_content].astype(str)
        return data

    def get_tweets(self, filter_pos_labs=False, filter_neg_labs=False, return_ids=False):
        if return_ids:
            cols = [self.tweet_content, self.tweet_id]
        else:
            cols=self.tweet_content

        if filter_neg_labs:
            return self.data[self.data[self.labcol]==1.0][cols].values
        elif filter_pos_labs:
            return self.data[self.data[self.labcol] != 1.0][cols].values
        else:
            return self.data[cols].values

    def get_clean_text(self, filter_pos_labs=False, filter_neg_labs=False, return_ids=True, spanish_flag=False):

        if  self.tknzed_tweets not in self.data.columns:
            print("going into here because tknzd tweets not in data column")
            X_tr = tokenize_tweets(self.get_tweets())
            self.data[self.tknzed_tweets] = X_tr
            print(np.shape(self.data[self.tknzed_tweets]))
            datatemp = self.data.dropna(subset=[self.tknzed_tweets])
            print(np.shape(datatemp[self.tknzed_tweets]))
            # self.data.to_csv( os.path.splitext(self.path2data)[0] + '_clean.csv'  )
            if spanish_flag:
                datatemp.to_csv( os.path.splitext(self.path2data)[0] + '_spanish_clean.csv'  )
            else:
                datatemp.to_csv( os.path.splitext(self.path2data)[0] + '_clean.csv'  )
            # return X_tr


        if return_ids:
            print('All the columns in the current dataset: ', self.data.columns)
            cols = self.data.columns
        else:
            cols= self.tknzed_tweets

        if filter_neg_labs:
            return self.data[self.data[self.labcol]==1.0][cols]
        elif filter_pos_labs:
            return self.data[self.data[self.labcol] != 1.0][cols]
        else:
            return self.data[cols]

    def get_labels(self):
        return self.data[self.labcol].values

    # check weather the tweet has an url link or not
    def dataset_statistics(self):
        no_tweets_with_url = 0
        lbl_data = self.data[self.data[self.labcol] == 1]['text_clean']
        for t in lbl_data:
            if '<url>' in t:
                no_tweets_with_url += 1

        print('there are:', no_tweets_with_url, 'with urls. Out of:', len(lbl_data))

class VaxDataReader(DataReader):
    def __init__(self, datapath, vax_file = 'labeled_vax.csv', label_column='Pro.Anti', tweet_content='Contents', override_labels=False):
        super().__init__(os.path.join(datapath, vax_file), label_column=label_column, tweet_content=tweet_content, override_lab_col=override_labels)
        return

    #changes the labeling
    def get_labels(self):
        self.data.dropna(subset = [self.labcol], inplace=True)
        print(self.data[self.labcol])
        self.data[self.labcol] = self.data[self.labcol].str.lower()
        # class labels 1- anti vaxcine 2- pro vaxcine 0-other
        mapping = {self.labcol: {'a': 1, 'p': 2, 'x':0}}
        self.data = self.data.replace(mapping)
        print( self.data[self.labcol]    )
        return self.data[self.labcol].values


# 1. check which words we use, and which words we don't use...
# 2. The words we don't use we dont need to save them since we will not use them for any experiments
# 3. For the words we use but are not in the glove file we need to create new embeddings (?)
class GloveEmbeddings:
    def __init__(self, path2data=''):
        self.path2data = path2data
        self.vocab = {}
        self.inv_vocab = {}
        self.tot_words = 0
        #self.emb = np.zeros()
        return

    def read(self):
        pprefix = "[" + self.__class__.__name__ +"] (read):"
        t0 = time.time()
        with open (self.path2data) as f:
            #for i, line in enumerate(csv.reader(f, delimiter=' ')):
            for i, entry in enumerate(f):
                line = entry.strip().split()
                if line[0] not in self.vocab:
                    self.vocab[line[0]] = self.tot_words
                    self.inv_vocab[self.tot_words] = line[0]
                    self.tot_words += 1
                else:
                    print(pprefix, 'multi-entry', i, line[0])
                #print(pprefix, i, line[0])
                #print(pprefix, line)

        print_elapsed_time(pprefix, time.time()-t0)
        #print(pprefix, self.vocab['daughter'])

    #given a list of tweets?
    def transform(self, X):
        pprefix = "[" + self.__class__.__name__ + "] (transform):"
        new_X = []
        unk_vocab = {}
        unk_vocab_freq = {}
        unk_tot = 0

        for tweet in clean_tweets(X):
            new_t = []
            for word in tweet:
                if word in self.vocab:
                    new_t.append(self.vocab[word])
                else:
                    # check if its a hyphenated term, its possible that the non-hyphenated term exists
                    if '-' in word:
                        if word.replace('-', "") in self.vocab:
                            new_t.append(self.vocab[word.replace('-', "")])
                        else: # add the individual terms of the non-hyphenated phrase
                            for term in word.split('-'):
                                if term in self.vocab:
                                    new_t.append(self.vocab[term])
                                # otherwise add to list of uknown terms
                                # TODO: Assign unk an index in the vocab
                                elif term not in unk_vocab:
                                    unk_vocab[term] = unk_tot
                                    unk_tot += 1
                                    unk_vocab_freq[term] =1
                                    print(pprefix, 'no embedding for:', term, 'found in phrase', word)
                                elif term in unk_vocab:
                                    unk_vocab_freq[term] += 1

                    if word not in unk_vocab:
                        unk_vocab[word] = unk_tot
                        unk_vocab_freq[word] = 1
                        unk_tot += 1
                        #print(pprefix, 'no embedding for:', word)
                    elif word in unk_vocab_freq:
                        unk_vocab_freq[word] += 1

            new_X.append(new_t)

        for word in unk_vocab_freq:
            if unk_vocab_freq[word] > 1:
                print(pprefix, 'no embedding for:', word, '\t\t\t\twith frequency:', unk_vocab_freq[word])

        print (pprefix, 'number of unk words', unk_tot)
        return new_X


# This method generates a training/testing dataset for the given labeled dataset
def prepare_train_test_vax(path2data='data/vax_data/'):
    # dr = VaxDataReader('data/', antiVfile='antivax_from_2017-09-01_to_2018-08-31.csv',
    #                    proVfile='provax_from_2017-09-01_to_2018-08-31.csv')

    # starting from the raw files first combine both files
    anti = pd.read_csv(os.path.join(path2data, 'antivax_from_2017-09-01_to_2018-08-31.csv'))
    pro = pd.read_csv(os.path.join(path2data, 'provax_from_2017-09-01_to_2018-08-31.csv'))

    data = pd.concat([anti, pro])

    # save the csv file to prerpocess using VaxDataReader
    data.to_csv( os.path.join(path2data, 'labeled_vax.csv'))
    dr = VaxDataReader (path2data, 'labeled_vax.csv')
    y, X = dr.get_labels(), dr.get_clean_text()

    print(np.shape(y))
    print(np.shape(X), X[0])

    # finally create the train/test split
    final = pd.read_csv( os.path.join(path2data, 'labeled_vax_clean.csv'))
    msk = np.random.rand(len(final)) < 0.8
    train = final[msk]
    test = final[~msk]

    train.to_csv(os.path.join(path2data, 'train_vax.csv'))
    test.to_csv(os.path.join(path2data, 'test_vax.csv'))

    # ge = GloveEmbeddings()
    # ge.read()
    #
    # new_X = ge.transform(X)
    # print(new_X[0])

# given a tweet file... preprocess and saves to be able to apply the classification model to
def prepare_classification_data():
    path2data = 'data/vax_data/'
    # post = pd.read_csv(os.path.join(path2data, 'Posts_from_2018-09-01_to_2019-03-18_master.csv'))
    # dr = VaxDataReader(path2data, 'Posts_from_2018-09-01_to_2019-03-18_master.csv')
    dr = VaxDataReader(path2data, 'proantivax_from_2017-09-01_to_2018_08-31_not4lstm.csv', override_labels=True)
    X = dr.get_clean_text()

    print(np.shape(X))

# given an instagram file... process and saves to be able to apply the classification model
def prepare_instagram_data():
    # path2data='data/instagram/idataset4_all_feb22.csv'
    path2data='data/instagram/hiv_data/hiv1920_all_no_dups.csv'
    dr = DataReader(path2data, label_column='content_behavior', tweet_content = 'Contents',
                    tweet_id = 'img_ids', override_lab_col = False)
    X = dr.get_clean_text()
    print(np.shape(X))

def prepare_data(path2data='data/202004/expert_tweets.csv', label_column = 'content_behavior',
                 tweet_content='Content', tweet_id='id', spanish_flag=False):

    dr = DataReader(path2data, label_column=label_column, tweet_content = tweet_content,
                    tweet_id = tweet_id, override_lab_col = False)
    X = dr.get_clean_text(spanish_flag=spanish_flag)
    print('type of x: ', type(X))
    y = dr.get_labels()
    print(np.shape(X))
    print(np.shape(y))
    print("The above 2 should be the same")

    print(X.iloc[0])
    ## so now we have the labels and the clean tweets
    ## we need to used the glove embeddings to make text -> floats

    print("the value of spanish_flag::: ", spanish_flag)
    if spanish_flag:
        ge = GloveEmbeddings(path2data="../../data/embeddings/spanish/SBW-vectors-300-min5.txt")
#        ge = GloveEmbeddings(path2data="/Users/priyankadey/Desktop/stuff/research/HIV Spanish Actionable Tweets/hiv-spanishactionabilityclassifier/data/embeddings/spanish/SBW-vectors-300-min5.txt")
        ge.read()
        new_X = ge.transform(X['preproc_tweets'].values)
        X['Spanish Embedded Tweet'] = new_X
        print(X.iloc[0])
#        X.to_csv("/Users/priyankadey/Desktop/stuff/research/HIV Spanish Actionable Tweets/hiv-spanishactionabilityclassifier/data/es_data/spanish_embedded_tweets.csv")
        X.to_csv("../../data/es_data/valid_data_spanish_embedded_tweets.csv") 
    else:
        ge = GloveEmbeddings(path2data="/Users/priyankadey/Desktop/stuff/research/HIV Spanish Actionable Tweets/hiv-spanishactionabilityclassifier/data/embeddings/glove/glove.twitter.27B.25d.txt")
        ge.read()
        ## we pass in just the preprocessed data into the transformer function
        new_X = ge.transform(X['preproc_tweets'].values)
        print(new_X[0])
        print(type(new_X))
        X['Glove Embedded Tweet'] = new_X
        print(X.iloc[0])
        ### save the data to some files
        X.to_csv("/Users/priyankadey/Desktop/stuff/research/HIV Spanish Actionable Tweets/hiv-spanishactionabilityclassifier/data/en_data/english_glove_embedded_tweets.csv")

def debug():
    # ge = GloveEmbeddings()
    # ge.read()
    dr = DataReader('data/behavior_tweets_tr.csv')
    dr.dataset_statistics()
    dr = DataReader('data/behavior_tweets_dev.csv')
    dr.dataset_statistics()

    # X, y = dr.get_text(return_ids=False), dr.get_labels()

    # new_X = ge.transform(X)

    # X_tr = tokenize_tweets(X)
    # dr.data['text_clean'] = X_tr
    # dr.data.to_csv('data/behavior_tweets_dev_clean.csv')

    # print(X[:10])
    #print(X_tr[:10])

import argparse

def parse_input():

    parser = argparse.ArgumentParser(description='Recompute Classification labels for developed model and post-process data.', prefix_chars='-+')
    parser.add_argument('--datapath', dest='path2data', default='',
                        help='[REQUIRED] The datapath for which we want to preprocess, i.e. for which we want to label using the classifier.')
    parser.add_argument('--inputFile', dest='input_file', default='',
                        help='Input file which we are preprocessing and adding labels to. The outputfile = [inputfile]_clean.csv')
    # parser.add_argument('--outputfile', dest='output_file', default='',
    #                     help='Input file which we are preprocessing and adding labels to.'
    parser.add_argument('--prepareData', dest='prepare_data', action='store_true', default = False,
                        help='Use this flag if you intend to prepare data for classification.')
    parser.add_argument('--columnLabel', dest='column_label', default='content_behavior',
                        help='The name of the label to classify. (default: content_behavior)')
    parser.add_argument('--tweetContent', dest='tweet_content', default='Content',
                        help='The name of the column in the [inputfile] containing the text content. (default: Content)')
    parser.add_argument('--tweetID', dest='tweet_id', default='id',
                        help='The name of the column which contains the unique identifier for the example input. (default: id)')
    parser.add_argument('--spanish', dest='spanish_flag', action='store_true', default=False)

    args = parser.parse_args()
    return args

    # python3 tokenizer.py --prepareData --columnLabel content_behavior --tweetContent translated_tweet  --tweetID tweet_id  --spanish --inputFile final_joined_translated_data.csv --datapath /Users/priyankadey/Desktop/stuff/research/HIV\ Spanish\ Actionable\ Tweets/hiv-spanishactionabilityclassifier/data/deepl


if __name__== '__main__':
    # prepare_classification_data()
    # prepare_instagram_data()
    print("we are in the main")
    args = parse_input()
    print(args.spanish_flag)
    print("print the spanish arg before")
    if args.prepare_data:
        prepare_data(path2data=os.path.join(args.path2data, args.input_file), label_column = args.column_label,
                 tweet_content=args.tweet_content, tweet_id=args.tweet_id, spanish_flag=args.spanish_flag)
