import sys
import re
import hashlib
import os
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from nltk.tokenize import TweetTokenizer
import ast

FLAGS = re.MULTILINE | re.DOTALL #| re.LOCALE
RANDOMSEED=69420


def print_elapsed_time(pprefix, time_elapsed):
    #time_elapsed = time.time()-start_time
    m, s = divmod(time_elapsed, 60)
    h, m = divmod(m, 60)
    print(pprefix, "Total time elapsed: %d:%02d:%02d (%.2fs)" %(h, m, s, time_elapsed), sep="")

def merge_data(label_file, tweet_file):
    label_data = pd.read_csv(label_file)
    tweet_data = pd.read_csv(tweet_file)
    label_data = label_data.dropna(subset=['url', 'content_behavior'])
    ids = [ str(url).split('/')[-1] for url in label_data['url'].values]
    label_data['id'] = ids

    label_data['id'] = label_data['id'].apply(pd.to_numeric)
    tweet_data = tweet_data.dropna(subset=['id'])
    tweet_data['id'] = tweet_data['id'].apply(pd.to_numeric,errors='ignore')

    iids = [int(i) for i in ids]
    tweet_data = tweet_data[tweet_data['id'].isin(iids)]

    all_data = pd.merge(label_data,tweet_data[['text','id', 'created_at','handle',
                                               'retweet_count', 'favorite_count','dump_at']], how='inner', on='id')

    all_data = all_data.drop(['Unnamed: 0'],axis=1)
    all_data.to_csv('data/behavior_tweets.csv')

def gen_tr_dev_splits(data, label_data='content_behavior', drop_na=True):
    # data = pd.read_csv(datafile, encoding='utf-8')
    # data['text'] = data['text'].apply(ast.literal_eval)
    if drop_na:
        data = data[~data[label_data].isnull()]

    # clean_tweets = np.array([ t.decode('utf-8') for t in data['text'].values ])
    ### does everything except the unk token (remove infrequent terms???)
    clean_tweets = [ t for t in data['text'].values]
    tok_tweets = tokenize_tweets(clean_tweets)
    data[label_data] = data[label_data].astype(int)
    data[label_data] = data[label_data].clip(lower=0)
    data['text_clean'] = tok_tweets
    data = remove_dups(data, label_data = label_data)

    y = data[label_data].values
    X = [ idx  for idx in range(np.shape(data['text'].values)[0]) ]

    # train_test_split(X,y)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.10, random_state = RANDOMSEED)

    df_train = data.iloc[X_train]
    df_test = data.iloc[X_test]

    return df_train, df_test

    # df_train.to_csv('data/behavior_tweets_tr_v2.csv')
    # df_test.to_csv('data/behavior_tweets_dev_v2.csv')

def remove_dups(df, label_data='content_behavior'):
    tweets = df['text_clean'].values
    labels = df[label_data].values
    ids = df['id'].values
    hashtable = {}
    duplicates = []

    for i, (t, y, tid) in enumerate(zip(tweets, labels, ids)):
        hash_key = hashlib.sha224(t.encode()).hexdigest()
        if hash_key not in hashtable:
            hashtable[hash_key] = {'str': t, 'idx': [i], 'labels': [y], 't_ids': [tid]}
        else:
            hashtable[hash_key]['idx'].append(i)
            hashtable[hash_key]['labels'].append(y)
            hashtable[hash_key]['t_ids'].append(tid)
            duplicates.append(hash_key)
            #

    unique_keys = list(set(hashtable.keys()) - set(duplicates))
    valid_concensus = []

    for key in set(duplicates):
        if sum(hashtable[key]['labels']) != len(hashtable[key]['labels']) and sum(hashtable[key]['labels']) != 0:
            continue

        valid_concensus.append(key)

    valid_keys = unique_keys + valid_concensus

    valid_idxs = []
    for key in valid_keys:
        valid_idxs.append(hashtable[key]['idx'][0])

    # print(valid_idxs)
    # print(df.count())
    return df.iloc[valid_idxs]

"""
preprocess-twitter.py
python preprocess-twitter.py "Some random text with #hashtags, @mentions and http://t.co/kdjfkdjf (links). :)"
Script for preprocessing tweets by Romain Paulus
with small modifications by Jeffrey Pennington
with translation to Python by Motoki Wu
Translation of Ruby script to create features for GloVe vectors for Twitter data.
http://nlp.stanford.edu/projects/glove/preprocess-twitter.rb
"""
def camel_case_split(identifier):
    matches = re.finditer('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)', identifier)
    return [m.group(0) for m in matches]

def hashtag(text):
    text = text.group()
    hashtag_body = text[1:]
    if hashtag_body.isupper():
        result = "<hashtag> {} <allcaps>".format(hashtag_body.lower())
    else:
        result = " ".join(["<hashtag>"] + camel_case_split(hashtag_body))
    return result

def allcaps(text):
    text = text.group()
    return text.lower() + " <allcaps>"


def tokenize(text):
    # Different regex parts for smiley faces
    eyes = r"[8:=;]"
    nose = r"['`\-]?"

    # function so code less repetitive
    def re_sub(pattern, repl):
        return re.sub(pattern, repl, text, flags=FLAGS)

    print("the text to tokenize")
    print(text)

    text = re_sub(r"\r", " ")
    text = re_sub(r"https?:\/\/\S+\b|www\.(\w+\.)+\S*", "<url>")
    text = re_sub(r"@\w+", "<user>")
    text = re_sub(r"{}{}[)dD]+|[)dD]+{}{}".format(eyes, nose, nose, eyes), "<smile>")
    text = re_sub(r"{}{}p+".format(eyes, nose), "<lolface>")
    text = re_sub(r"{}{}\(+|\)+{}{}".format(eyes, nose, nose, eyes), "<sadface>")
    text = re_sub(r"{}{}[\/|l*]".format(eyes, nose), "<neutralface>")
    text = re_sub(r"/"," / ")
    text = re_sub(r"<3","<heart>")
    text = re_sub(r"[-+]?[.\d]*[\d]+[:,.\d]*", "<number>")
    text = re_sub(r"#\S+", hashtag)
    text = re_sub(r"([!?.]){2,}", r"\1 <repeat>")
    text = re_sub(r"\b(\S*?)(.)\2{2,}\b", r"\1\2 <elong>")

    ## -- I just don't understand why the Ruby script adds <allcaps> to everything so I limited the selection.
    #text = re_sub(r"([^a-z0-9()<>'`\-]){2,}", allcaps)
    text = re_sub(r"([A-Z]){2,}", allcaps)

    return text.lower()


def prep_raw_data(coding_file='Fall17_Codings_Combined.v20180501.csv'):
    lab_file = os.path.join('data/', coding_file)
    raw_file = os.path.join('data/', 'IndOrg_NotUsed_062817.csv')
    merge_data(lab_file, raw_file)
    gen_tr_dev_splits('data/behavior_tweets.csv')

# tokenize a list of tweets
def tokenize_tweets(list_of_tweets):
    return [ tokenize(t) for t in list_of_tweets]

# TODO: use https://github.com/erikavaris/tokenizer/tree/master/tokenizer instead here
tknzr = TweetTokenizer()
def tweet_regularizer(tweet):
    t = tweet.replace("'", "")
    t = t.replace("-", " ")
    new_tweet=t
    return new_tweet

def clean_tweets(tweets):
    return [ tknzr.tokenize(tweet_regularizer(tweet)) for tweet in tweets ]


def debug():
    _, text = sys.argv
    if text == "test":
        text = "I TEST alllll kinds of #hashtags and #HASHTAGS, @mentions and 3000 (http://t.co/dkfjkdf). w/ <3 :) haha!!!!!"
    tokens = tokenize(text)
    print(tokens)
    return

# if __name__=='__main__':
#     debug()

if __name__ == '__main__':
    prep_raw_data(coding_file='Fall17_Codings_Combined.v20180611.csv')
