**To Do:**

TRANSLATION OF TWEETS IS COMPLETE, please find full translated data in data/deepl/translated_data.csv 


We need to translate some more tweets. Please follow the process below for translating tweets via the DeepL api. 

1) Create a DeepL Pro Account (free version). You will have to enter your credit card details but they will not be charged unless you manually upgrade your API version. 

2) Change the API Key variable to the one you have received in deepl/deepl_translate.py 

3) You will be prompted to enter an index to start translating from (enter after 3025). 

4) This will generate a file like translated_tweets_1690_to_1767.csv, we still need to join this with another file to get which tweets are actionable. 

5) I can join the files once we have translated more data. 

We need to build some baseline models:

  1) Priyanka: SVM, LogR

  2) Mandy: BERT, LSTM

We should also look into BiLSTM maybe

~ Feel free to edit below section ~

Some additional tasks: 

1) Priyanka: 

  - Research on studying emojis and other links/images in classification tasks 

  - Back-translation approach (using Rouge scores to select the best tweets to use for validation/testing)
	
2) Mandy/Sarah: 

  - Decide on some tweets (~500) that give highest classification scores to send to Spanish HIV annotators/researchers for ground truth labeling
