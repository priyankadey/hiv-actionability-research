import load_dataset
import models

import pandas as pd
import argparse

def main():
    parser = argparse.ArgumentParser(description='Run Experiments for English & Spanish Data', prefix_chars='-+')
    # by default, uses spanish data
    parser.add_argument('--english', dest='english_flag', action='store_true', default=False,
                            help='Use this flag if you want to run on English data')

    parser.add_argument('--favs', dest='favs', default='none',
                            help='raw for using raw counts, thresh for using thresh vals, not used by default')
    parser.add_argument('--tones', dest='tones', default='none',
                            help='raw for using raw counts, thresh for using thresh vals, not used by default')

    parser.add_argument('--N_k', dest='nk', default=10, type=int,
                            help='take top N_k ranked actionable messages (used when calculating scoring metrics)')
   
    parser.add_argument('--model', dest='model', default='all', 
                            help='the ML classifier to train and test, options: svm, logr, xgboost') 

    args = parser.parse_args()

    mode = "es"
    if args.english_flag:
        ### run english dataset
        data = pd.read_csv("../data/en_data/english_glove_embedded_tweets.csv")
        mode = "en"
    else:
        ### spanish dataset
        data = pd.read_csv("../../hiv-spanishactionabilityclassifier/data/spanish_data_glove_tone_favs.csv")
        ### the politics/sports dataset 
        data = pd.read_csv("../data/df.csv") 
        ### the joined data -> original + alex validation dataset 
        data = pd.read_csv("../data/joined_dataset.csv") 
        data = pd.read_csv("../data/add_features/favourites_count/full_dataset_with_favourites.csv")

    if args.favs == "raw":
        ## we need to remove the tweets that have NA vals
        data = data[data['Favourites'].notna()]

    ### get dataset from load_dataset
    train, test, train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad = load_dataset.stratified_train_test_split(data, args.favs, args.tones, mode)
    ### run the specified model
    if args.model == 'svm':
        models.SVM(train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, args.favs, args.tones, args.nk)
    elif args.model == 'xgboost':
        models.xgboost(train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, args.favs, args.tones, args.nk)
    elif args.model == 'logr': 
        models.logR(train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, args.favs, args.tones, args.nk)
    else: 
        ### run all the models 
        models.SVM(train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, args.favs, args.tones, args.nk)
        print("Results for the SVM were printed above") 
        models.xgboost(train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, args.favs, args.tones, args.nk)
        print("Results for xgboost were printed above") 
        #models.logR(train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, args.favs, args.tones, args.nk)
        #print("Results for logR were printed above") 

main()
