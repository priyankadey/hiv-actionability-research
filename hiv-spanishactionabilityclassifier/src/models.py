### for now, a placeholder function to do 10-fold cross validation
### need to incorporate the neural net models here eventually
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, precision_recall_fscore_support, precision_score
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn import svm
from xgboost import XGBClassifier

## helper to add new feature to existing dataset
def add_feat_to_data(favs, data):
    res = []
    for i, elem in enumerate(data):
        z = np.append(elem, favs[i])
        res.append(z)

    return np.array(res)

def cross_valid_run(mod,train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, favs, tones, N_k=10):

    fa_ranks = []

    precisions = []
    recalls = []
    #f_scores = []
    avg_precs_k = []

    fewer_than_N_k_count = 0

    for i in range(10):
        cur_train_X_i = train_X[i]
        cur_test_X_i = test_X[i]

        if favs != "none":
            cur_train_X_i = add_feat_to_data(train_X_favs[i], train_X[i])
            cur_test_X_i = add_feat_to_data(test_X_favs[i], test_X[i])
#         cur_train_X_i = train_X[i]
        ## instead of using all scores we use thresholds for the tones
        if tones == 'thresh':
            cur_train_X_i = add_feat_to_data(train_X_conf[i], cur_train_X_i)
            cur_train_X_i = add_feat_to_data(train_X_tent[i], cur_train_X_i)
            cur_test_X_i = add_feat_to_data(test_X_conf[i], cur_test_X_i)
            cur_test_X_i = add_feat_to_data(test_X_tent[i], cur_test_X_i)
        elif tones == 'raw':
            cur_train_X_i = add_feat_to_data(train_X_fear[i], cur_train_X_i)
            cur_train_X_i = add_feat_to_data(train_X_conf[i], cur_train_X_i)
            cur_train_X_i = add_feat_to_data(train_X_tent[i], cur_train_X_i)
            cur_train_X_i = add_feat_to_data(train_X_ana[i], cur_train_X_i)
            cur_train_X_i = add_feat_to_data(train_X_ang[i], cur_train_X_i)
            cur_train_X_i = add_feat_to_data(train_X_joy[i], cur_train_X_i)
            cur_train_X_i = add_feat_to_data(train_X_sad[i], cur_train_X_i)
            cur_test_X_i = add_feat_to_data(test_X_fear[i], cur_test_X_i)
            cur_test_X_i = add_feat_to_data(test_X_conf[i], cur_test_X_i)
            cur_test_X_i = add_feat_to_data(test_X_tent[i], cur_test_X_i)
            cur_test_X_i = add_feat_to_data(test_X_ana[i], cur_test_X_i)
            cur_test_X_i = add_feat_to_data(test_X_ang[i], cur_test_X_i)
            cur_test_X_i = add_feat_to_data(test_X_joy[i], cur_test_X_i)
            cur_test_X_i = add_feat_to_data(test_X_sad[i], cur_test_X_i)
        print("just above the training model part") 
        print(type(train_Y[i]))
        ### change -1 to 0 bc xgboost needs 0 - 1 - 2 - 3 - etc. 
        train_Y_i = train_Y[i] > 0
        train_Y_i = train_Y_i.astype(int)
        print(train_Y_i)
        mod.fit(cur_train_X_i, train_Y_i)
        print('finished training model')
        predictions_mod = mod.predict(cur_test_X_i)


        ### need to fix calculation of this bottom section
        probs = mod.predict_proba(cur_test_X_i)
        actionable_probs = []
        non_actionable_probs = []
        ## the higher of the two probs
        higher_probs = []
        ## the predicted label
        pred_labels = []
        probs_list = list(probs)
        for elem in probs:
            if elem[1] >= elem[0]:
                pred_labels.append(1)
                higher_probs.append(elem[1])
            else:
                ### might need to change this to appending 0 b/c xgboost does not accept neg labels 
                pred_labels.append(-1)
                higher_probs.append(elem[0])
            actionable_probs.append(elem[1])
            non_actionable_probs.append(elem[0])
        
        print("CLASSIFICATION REPORT RESULTS FOR : ", i)
        print(classification_report(list(test_Y[i]), pred_labels))

        ## we need to identify the value of n (because 10 is sometimes too high drops below 0.5 sometimes then)
        sorted_ = sorted(actionable_probs, reverse=True)
        na_sorted = sorted(non_actionable_probs, reverse=True)

        ### use these tweets to calculate the precision recall for the actionable tweets
        ranked = (np.argsort(actionable_probs))
        largest_indices = ranked[::-1][:N_k]
        ## official labels
        labels_ = test_Y[i][largest_indices]
        pred_labels_ = [pred_labels[i] for i in largest_indices]

        ## calculate rank of first actionable message
        try:
            fa_ranks.append(list(labels_).index(1.0))
        except:
            pass

        ## calculate precision
        # # of true actionable msgs/total msgs identified by classifier as actionable
        true_act = 0
        for ii in range(len(labels_)):
            if labels_[ii] == 1 and pred_labels_[ii] == 1:
                true_act+= 1
        #true_act = list(labels_).count(1)
        act_pred = list(pred_labels_).count(1)
        ### there are cases when N_k is higher than the actual number of actionable messages being predicted 
        ### in this case, we should keep track of this number & remove from the final metrics prediction?? 
        if true_act > act_pred:
                fewer_than_N_k_count += 1 
                continue 
        print("Precision: ", true_act/act_pred)
        print(len(labels_))
        print(len(pred_labels_))
        print(labels_)
        print(pred_labels_)
        print("Sklearn: ", precision_score(labels_, pred_labels_))
        precisions.append(true_act/act_pred)

        # recall: # of true actionable msgs/total actionable msgs in dataset
        #print(test_Y[i])
        total_act = list(test_Y[i]).count(1)
        print("Recall: ", true_act/total_act)
        recalls.append(true_act/total_act)
	
	# average precision @ k: loop from 1 to k to calculate the precision & take the mean 
        prec_k = []
        rel_docs = 0
        prec_calc = 0
        prev_prec_calc = 0
        for i in range(N_k):
                if labels_[i] == 1 and pred_labels_[i] == 1:
                        rel_docs += 1
                        true_act = list(labels_)[:i+1].count(1)
                        pred_act = list(pred_labels_)[:i+1].count(1)
                        prec_calc += true_act/pred_act
                true_act = list(labels_)[:i+1].count(1)
                pred_act = list(pred_labels_)[:i+1].count(1)
                prev_prec_calc += true_act/pred_act
        print("Average Precision @ K for this round: ", str(prec_calc/rel_docs))   
        print("Previous calculation:::: Average Precision @ K for this round: ", str(prev_prec_calc/N_k))   
        avg_precs_k.append(prec_calc/rel_docs)
        
    avg_ranks = sum(fa_ranks)/10 + 1
    effort_score = 1/avg_ranks

    print("avg ranks: ", avg_ranks)

    print("Effort score: ", effort_score)


    ## calculate the precision, recall, f-scores @ N scores
    for i in range(1):
        if i == 0:
            print("Scores for actionable data")
        else:
            print("Scores for non-actionable data")

        precision, recall, avg_prec_k = 0, 0, 0

        print("All the calculated precisions: ", str(precisions))
        for j in range (len(precisions)):
            precision += precisions[j]
            recall += recalls[j]
            avg_prec_k += avg_precs_k[j]
            # f += f_scores[j][i]
        print("Precision @ " + str(N_k) + ": ", str(precision/len(precisions)))
        print("Recall @ " + str(N_k) + ": ", str(recall/len(recalls)))
        print("Average Precision  @ " + str(N_k) + ": ", str(avg_prec_k/len(avg_precs_k)))
        print("Total count of runs with fewer than " + str(N_k) + " predicted actionable messages: " + str(fewer_than_N_k_count)) 
        # print("F-Score @ " + str(N_k) + ": ", str(f/10))
        

def logR(train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, favs, tones, N_k):
    log_R = LogisticRegression(verbose=1, solver='liblinear',random_state=0, C=5, penalty='l2',max_iter=1000)
    cross_valid_run(log_R, train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, favs, tones, N_k)

def SVM(train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, favs, tones, N_k):
    svm_ = svm.SVC(C=1.0, kernel='rbf', degree=5, gamma='auto', probability=True)
    cross_valid_run(svm_, train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, favs, tones, N_k)

def xgboost(train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, favs, tones, N_k):
    xg_boost = XGBClassifier() 
    cross_valid_run(xg_boost, train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad, favs, tones, N_k) 
