import ast
import numpy as np
import pandas as pd

# helper func to get 10 dfs of equal size
def split_data(df):
    split_dfs = []
    len_ = len(df)
    start = 0
    for i in range (10):
        if i == 9:
            df_ = df[start:]
        df_ = df[start: start + int(len_/10)]
        start += (int(len_/10))
        split_dfs.append(df_)
    return split_dfs

# remove tweets from test set if already in train set
def ps(test_list, train_list):
    removed_dups = []
    for elem in test_list:
        if elem not in train_list:
            removed_dups.append(elem)
    return removed_dups

# helper to convert fav to bool based on 75% percentile
def convert_fav_bool(favs):
    res = []
    for fav in favs:
        ## should not be raw numbr, but for now
        if fav >= 7756:
            res.append(1)
        else:
            res.append(0)
    return res

# convert confidence and tentative tones to bools based on 75% percentile
def convert_conf_bool(confs):
    res = []
    for c in confs:
        ## should not be raw numbr, but for now
        if c > 0.55:
            res.append(1)
        else:
            res.append(0)
    return res

def convert_tent_bool(tents):
    res = []
    for t in tents:
        ## should not be raw numbr, but for now
        if t > 0.6:
            res.append(1)
        else:
            res.append(0)
    return res

def stratified_train_test_split(data, favs, tones, mode):
    if mode == "es":
        col = 'Spanish Embedded Tweet'
    else:
        col = 'Glove Embedded Tweet'

    ## python stores the list as a string so we use literal eval to get back np array
    gt = [np.asarray(ast.literal_eval(t)) for t in data[col].values]
    print("type of data; ", type(data))

    ## find max length in gts
    ## we need to pad 0s to the embeddings b/c dif tweets have dif lens and thus dif embedding vector sizes
    gts = []
    len_ = (max([t.size for t in gt]))
    for t in gt:
        gts.append(np.pad(t, (0, (len_ - t.size)), 'constant').tolist())

    ## transform the values in content_behavior for xgboost (change -1 to 0) 
    #data.loc[data["content_behavior"] == -1] = 0

    ## split data into actionable and na for stratified split
    data['Glove_Tweet'] = gts
    print("glove tweet shape", data['Glove_Tweet'].shape)
    actionable = data[data['content_behavior'] == 1]
    not_actionable = data[data['content_behavior'] == -1]

    ## remove nas for favs
    if favs != "none":
        data = data[data['Favourites'].notna()]

    ### duplicate the underrepresented data
    ## 0.5 duplication
#     actionable = pd.concat([actionable, actionable.sample(frac=0.5)])
    ### end of duplication

    ## creates 10 divisions of this data to use for 10-fold cross validation
    act_data_split = split_data(actionable)
    ### we take only 10% of non-actionable data o.w. we get no results
    not_actionable_data = not_actionable.sample(frac=1)
    na_data_split = split_data(not_actionable_data)
    ### now we have the split data, we need to combine it for each cross validation iteration

    test = []
    train = []
    train_X = []
    train_Y = []
    test_X = []
    test_Y = []
    ### TO-DO: incorporate options for this in function
    test_X_favs = []
    train_X_favs = []
    test_X_fear = []
    train_X_fear = []
    test_X_conf = []
    train_X_conf = []
    test_X_tent = []
    train_X_tent = []
    test_X_ana = []
    train_X_ana = []
    test_X_ang = []
    train_X_ang = []
    test_X_joy = []
    train_X_joy = []
    test_X_sad = []
    train_X_sad = []

    for i in range (10):
        print("the beginning of the loop ")
        print(len(train_X_fear))
        te = pd.concat([act_data_split[i], na_data_split[i]])

        t = act_data_split[0:i]
        t.extend(act_data_split[i+1:])
        t.extend(na_data_split[0:i])
        t.extend(na_data_split[i+1:])
        t = pd.concat(t)
        t = t.sample(frac=1)

        te = te[~te.tweet_id.isin(t.tweet_id)]

        if mode == 'es':
            print("going into the spanish loop")
            if favs == 'thresh':
                ## using bool vals
                test_X_favs.append(convert_fav_bool(te['Favourites'].values))
                train_X_favs.append(convert_fav_bool(t['Favourites'].values))
            elif favs == 'raw':
                ## using raw values
                test_X_favs.append(te['Favourites'].values)
                train_X_favs.append(t['Favourites'].values)

#             favs = (te['Favourites'].values)
            # test_X_favs.append(favs)

            if tones == 'thresh':
                ### only use threshold values for conf & tent
                test_X_conf.append(convert_conf_bool(te['Confident'].values))
                test_X_tent.append(convert_tent_bool(te['Tentative'].values))
                train_X_conf.append(convert_conf_bool(t['Confident'].values))
                train_X_tent.append(convert_tent_bool(t['Tentative'].values))
            elif tones == 'raw':
                ### we want to incorporate all the tones
                print("the fear values")
                print(te['Fear'].values)
                test_X_fear.append(te['Fear'].values)
                test_X_sad.append(te['Sadness'].values)
                test_X_ana.append(te['Analytical'].values)
                test_X_ang.append(te['Anger'].values)
                test_X_tent.append(te['Tentative'].values)
                test_X_joy.append(te['Joy'].values)
                test_X_conf.append(te['Confident'].values)
                train_X_fear.append(t['Fear'].values)
                train_X_sad.append(t['Sadness'].values)
                train_X_ana.append(t['Analytical'].values)
                train_X_ang.append(t['Anger'].values)
                train_X_tent.append(t['Tentative'].values)
                train_X_joy.append(t['Joy'].values)
                train_X_conf.append(t['Confident'].values)
            ### only use threshold values for conf & tent
#             test_X_conf.append(convert_conf_bool(te['Confident'].values))
#             test_X_tent.append(convert_tent_bool(te['Tentative'].values))
#             test_X_favs.append(te['Favourites'].values)
            print('total length of embedded tweets:', len(te['Spanish Embedded Tweet'].values))
            test_X.append(te['Spanish Embedded Tweet'].values)
            train_X.append(t['Spanish Embedded Tweet'].values)
        else:
            print("mode not detected as es")
            test_X.append(te['Glove_Tweet'].values)
            train_X.append(t['Glove_Tweet'].values)

        ### transform these values to be 0 or 1?
        test_Y.append(te['content_behavior'].values)
        test.append(te)

        train_Y.append(t['content_behavior'].values)
        train.append(t)

    ### process the data into a 2D numpy array
    ### for some reason, this needs to be done for logR and SVM
    ### need to improve this section later
    for i in range(10):
        print('train_X[0] shape: ', train_X[i].shape)
        trainX_list = train_X[i].tolist()
        testX_list = test_X[i].tolist()
        trX = []
        teX = []
        print(trainX_list[0])
        print("printed the trainX_list above")
        if mode == 'es':
            trainX_nparray = [np.asarray(ast.literal_eval(elem)) for elem in trainX_list]
            testX_nparray = [np.asarray(ast.literal_eval(elem)) for elem in testX_list]
        else:
            trainX_nparray = [np.asarray((elem)) for elem in trainX_list]
            testX_nparray = [np.asarray((elem)) for elem in testX_list]
        len_ = max([t.size for t in trainX_nparray])
        len_test_ = max([t.size for t in testX_nparray])
        print("the training len: ", len_)
        print("the testing len: ", len_test_)
#         print("the maximum length: ", len_)
        for elem in trainX_nparray:
#             print('the size of the element before padding: ', elem.size)
            elem_array = np.pad(elem, (0, (50 - elem.size)), 'constant')
            trX.append(elem_array)
        for elem in testX_nparray:
#             print('the size of the element before padding: ', elem.size)
            elem_array = np.pad(elem, (0, (50 - elem.size)), 'constant')
            teX.append(elem_array)
#             print(len(elem_array))
        train_X[i] = np.stack(trX)
        test_X[i] = np.stack(teX)

    print("This is in the load dataset function and printing the train x fear length")
    print(len(train_X_fear))

    return train, test, train_X, train_Y, test_X, test_Y, train_X_favs, test_X_favs, test_X_fear, train_X_fear, test_X_conf,train_X_conf,test_X_tent,train_X_tent,test_X_ana,train_X_ana,test_X_ang,train_X_ang,test_X_joy,train_X_joy,test_X_sad, train_X_sad
